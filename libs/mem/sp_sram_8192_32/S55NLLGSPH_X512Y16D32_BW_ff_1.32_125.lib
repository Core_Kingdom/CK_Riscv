/*
    Copyright (c) 2023 SMIC
    Filename:      S55NLLGSPH_X512Y16D32_BW_ff_1.32_125.lib
    IP code:       S55NLLGSPH
    Version:       1.1.a
    CreateDate:    2023-2-15

    Synopsys Technology Library for General Single-PORT SRAM
    SMIC 55nm LL Logic Process

    Configuration: -instname S55NLLGSPH_X512Y16D32_BW -rows 512 -bits 32 -mux 16 
    Redundancy: Off
    Bit-Write: On
*/

/* DISCLAIMER                                                                      */
/*                                                                                 */  
/*   SMIC hereby provides the quality information to you but makes no claims,      */
/* promises or guarantees about the accuracy, completeness, or adequacy of the     */
/* information herein. The information contained herein is provided on an "AS IS"  */
/* basis without any warranty, and SMIC assumes no obligation to provide support   */
/* of any kind or otherwise maintain the information.                              */  
/*   SMIC disclaims any representation that the information does not infringe any  */
/* intellectual property rights or proprietary rights of any third parties. SMIC   */
/* makes no other warranty, whether express, implied or statutory as to any        */
/* matter whatsoever, including but not limited to the accuracy or sufficiency of  */
/* any information or the merchantability and fitness for a particular purpose.    */
/* Neither SMIC nor any of its representatives shall be liable for any cause of    */
/* action incurred to connect to this service.                                     */  
/*                                                                                 */
/* STATEMENT OF USE AND CONFIDENTIALITY                                            */  
/*                                                                                 */  
/*   The following/attached material contains confidential and proprietary         */  
/* information of SMIC. This material is based upon information which SMIC         */  
/* considers reliable, but SMIC neither represents nor warrants that such          */
/* information is accurate or complete, and it must not be relied upon as such.    */
/* This information was prepared for informational purposes and is for the use     */
/* by SMIC's customer only. SMIC reserves the right to make changes in the         */  
/* information at any time without notice.                                         */  
/*   No part of this information may be reproduced, transmitted, transcribed,      */  
/* stored in a retrieval system, or translated into any human or computer          */ 
/* language, in any form or by any means, electronic, mechanical, magnetic,        */  
/* optical, chemical, manual, or otherwise, without the prior written consent of   */
/* SMIC. Any unauthorized use or disclosure of this material is strictly           */  
/* prohibited and may be unlawful. By accepting this material, the receiving       */  
/* party shall be deemed to have acknowledged, accepted, and agreed to be bound    */
/* by the foregoing limitations and restrictions. Thank you.                       */  
/*                                                                                 */  

library(S55NLLGSPH_X512Y16D32_BW_ff_1.32_125) {
	delay_model		: table_lookup;
	time_unit		: "1ns";
	voltage_unit		: "1V";
	current_unit		: "1mA";
	leakage_power_unit	: "1mW";
	nom_process		: 1;
	nom_temperature		: 125;
	nom_voltage		: 1.32;
	capacitive_load_unit	 (1,pf);

	pulling_resistance_unit	        : "1kohm";

	/* additional header data */
	default_cell_leakage_power      : 0;
	default_fanout_load		: 1;
	default_inout_pin_cap		: 0.005;
	default_input_pin_cap		: 0.005;
	default_output_pin_cap		: 0.0;
	default_max_transition		: 0.500;

        /* default attributes */
        default_leakage_power_density : 0.0;
        slew_derate_from_library      : 1.000;
        slew_lower_threshold_pct_fall : 10.000;
        slew_upper_threshold_pct_fall : 90.000;
        slew_lower_threshold_pct_rise : 10.000;
        slew_upper_threshold_pct_rise : 90.000;
        input_threshold_pct_fall      : 50.000;
        input_threshold_pct_rise      : 50.000;
        output_threshold_pct_fall     : 50.000;
        output_threshold_pct_rise     : 50.000;

 	/* k-factors */
 	k_process_cell_fall             : 1;
 	k_process_cell_leakage_power    : 0;
 	k_process_cell_rise             : 1;
 	k_process_fall_transition       : 1;
 	k_process_hold_fall             : 1;
 	k_process_hold_rise             : 1;
 	k_process_internal_power        : 0;
 	k_process_min_pulse_width_high  : 1;
 	k_process_min_pulse_width_low   : 1;
 	k_process_pin_cap               : 0;
 	k_process_recovery_fall         : 1;
 	k_process_recovery_rise         : 1;
 	k_process_rise_transition       : 1;
 	k_process_setup_fall            : 1;
 	k_process_setup_rise            : 1;
 	k_process_wire_cap              : 0;
 	k_process_wire_res              : 0;
	k_temp_cell_fall		: 0.000;
	k_temp_cell_rise		: 0.000;
	k_temp_hold_fall                : 0.000;
	k_temp_hold_rise                : 0.000;
	k_temp_min_pulse_width_high     : 0.000;
	k_temp_min_pulse_width_low      : 0.000;
	k_temp_min_period               : 0.000;
	k_temp_rise_propagation         : 0.000;
	k_temp_fall_propagation         : 0.000;
	k_temp_rise_transition          : 0.0;
	k_temp_fall_transition          : 0.0;
	k_temp_recovery_fall            : 0.000;
	k_temp_recovery_rise            : 0.000;
	k_temp_setup_fall               : 0.000;
	k_temp_setup_rise               : 0.000;
	k_volt_cell_fall                : 0.000;
	k_volt_cell_rise                : 0.000;
	k_volt_hold_fall                : 0.000;
	k_volt_hold_rise                : 0.000;
	k_volt_min_pulse_width_high     : 0.000;
	k_volt_min_pulse_width_low      : 0.000;
	k_volt_min_period               : 0.000;
	k_volt_rise_propagation         : 0.000;
	k_volt_fall_propagation         : 0.000;
	k_volt_rise_transition	        : 0.0;
	k_volt_fall_transition	        : 0.0;
	k_volt_recovery_fall            : 0.000;
	k_volt_recovery_rise            : 0.000;
	k_volt_setup_fall               : 0.000;
	k_volt_setup_rise               : 0.000;


        operating_conditions(ff_1.32_125) {
		process	         : 1;
		temperature	 : 125;
		voltage	         : 1.32;
		tree_type	 : balanced_tree;
	}
        default_operating_conditions : ff_1.32_125;
	wire_load("sample") {
		resistance	 : 1.6e-05;
		capacitance	 : 0.0002;
		area	 : 1.7;
  		slope	 : 500;
		fanout_length	 (1,500);
	}
        lu_table_template(S55NLLGSPH_X512Y16D32_BW_bist_mux_delay_template) {
           variable_1 : input_net_transition;
           variable_2 : total_output_net_capacitance;
               index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
               index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        lu_table_template(S55NLLGSPH_X512Y16D32_BW_mux_mem_out_delay_template) {
           variable_1 : input_net_transition;
           variable_2 : total_output_net_capacitance;
               index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
               index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        lu_table_template(S55NLLGSPH_X512Y16D32_BW_mem_out_delay_template) {
           variable_1 : input_net_transition;
           variable_2 : total_output_net_capacitance;
               index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
               index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
	lu_table_template(S55NLLGSPH_X512Y16D32_BW_bist_mux_slew_template) {
           variable_1 : total_output_net_capacitance;
           index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
	lu_table_template(S55NLLGSPH_X512Y16D32_BW_mem_out_slew_template) {
           variable_1 : total_output_net_capacitance;
           index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
	lu_table_template(S55NLLGSPH_X512Y16D32_BW_clk_setup_constraint_template) {
           variable_1 : related_pin_transition;
           variable_2 : constrained_pin_transition;
               index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
               index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
	lu_table_template(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
           variable_1 : related_pin_transition;
           variable_2 : constrained_pin_transition;
           index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
           index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        power_lut_template(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew_load) {
           variable_1 : input_transition_time;
           variable_2 : total_output_net_capacitance;
               index_1 ("1000, 1001");
               index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        power_lut_template(S55NLLGSPH_X512Y16D32_BW_energy_template_sigslew_load) {
           variable_1 : input_transition_time;
           variable_2 : total_output_net_capacitance;
           index_1 ("1000, 1001");
           index_2 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        power_lut_template(S55NLLGSPH_X512Y16D32_BW_energy_template_load) {
           variable_1 : total_output_net_capacitance;
           index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        power_lut_template(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
           variable_1 : input_transition_time;
               index_1 ("1000, 1001");
        }
        power_lut_template(S55NLLGSPH_X512Y16D32_BW_energy_template_sigslew) {
           variable_1 : input_transition_time;
               index_1 ("1000, 1001");
        }
	library_features(report_delay_calculation);
	type (S55NLLGSPH_X512Y16D32_BW_DATA) {
		base_type : array ;
		data_type : bit ;
		bit_width : 32;
		bit_from : 31;
		bit_to : 0 ;
		downto : true ;
	}
	type (S55NLLGSPH_X512Y16D32_BW_ADDRESS) {
		base_type : array ;
		data_type : bit ;
		bit_width : 13;
		bit_from : 12;
		bit_to : 0 ;
		downto : true ;
	}
	type (S55NLLGSPH_X512Y16D32_BW_S) {
		base_type : array ;
		data_type : bit ;
		bit_width : 4;
		bit_from : 3;
		bit_to : 0 ;
		downto : true ;
	}

	type (S55NLLGSPH_X512Y16D32_BW_WRITE) {
		base_type : array ;
		data_type : bit ;
		bit_width : 32;
		bit_from : 31;
		bit_to : 0 ;
		downto : true ;
	}

cell(S55NLLGSPH_X512Y16D32_BW) {
	area		 : 174788.437;
	dont_use	 : TRUE;
	dont_touch	 : TRUE;
        interface_timing : TRUE;
	memory() {
		type : ram;
		address_width : 13;
		word_width : 32;
	}
        bus(Q)  {
                bus_type : S55NLLGSPH_X512Y16D32_BW_DATA;
		direction : output;
		max_capacitance : 0.400;
                memory_read() {
			address : A;
		}
                timing() {
                        related_pin :   "CLK" ;
                        timing_type : rising_edge ;
                        timing_sense : non_unate;
                        cell_rise(S55NLLGSPH_X512Y16D32_BW_mem_out_delay_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.001, 0.005, 0.010, 0.050, 0.100, 0.200, 0.400");
                        values(\
			"0.614, 0.619, 0.623, 0.653, 0.687, 0.757, 0.897",\
			"0.615, 0.619, 0.624, 0.653, 0.688, 0.758, 0.897",\
			"0.614, 0.619, 0.623, 0.652, 0.687, 0.757, 0.897",\
			"0.610, 0.614, 0.618, 0.648, 0.682, 0.752, 0.892",\
			"0.594, 0.598, 0.602, 0.632, 0.667, 0.736, 0.876",\
			"0.553, 0.557, 0.562, 0.591, 0.626, 0.696, 0.835",\
			"0.497, 0.501, 0.505, 0.535, 0.570, 0.639, 0.779"\
                        );
                        }
                        rise_transition(S55NLLGSPH_X512Y16D32_BW_mem_out_slew_template) {
                        index_1("0.001, 0.005, 0.010, 0.050, 0.100, 0.200, 0.400");
			values("0.015, 0.014, 0.028, 0.086, 0.162, 0.317, 0.624");
                        }
                        cell_fall(S55NLLGSPH_X512Y16D32_BW_mem_out_delay_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.001, 0.005, 0.010, 0.050, 0.100, 0.200, 0.400");
                        values(\
			"0.608, 0.613, 0.618, 0.648, 0.682, 0.752, 0.892",\
			"0.609, 0.614, 0.619, 0.648, 0.683, 0.753, 0.893",\
			"0.608, 0.613, 0.618, 0.647, 0.682, 0.752, 0.892",\
			"0.604, 0.608, 0.613, 0.643, 0.677, 0.747, 0.888",\
			"0.587, 0.592, 0.597, 0.627, 0.662, 0.731, 0.872",\
			"0.547, 0.552, 0.557, 0.586, 0.621, 0.691, 0.831",\
			"0.491, 0.495, 0.500, 0.530, 0.565, 0.634, 0.775"\
                        );
                        }
                        fall_transition(S55NLLGSPH_X512Y16D32_BW_mem_out_slew_template) {
                        index_1("0.001, 0.005, 0.010, 0.050, 0.100, 0.200, 0.400");
			values("0.019, 0.018, 0.030, 0.081, 0.149, 0.291, 0.576");
                        }
                }
        }
        pin(CLK)   {
		direction : input;
		capacitance : 0.034;
                clock : true;
                max_transition       : 0.500;
                min_pulse_width_high : 0.020 ;
                min_pulse_width_low  : 0.099 ;
                min_period           : 0.675 ;


                internal_power(){
                        when : "(!CEN & ! \
                                   WEN & ! \
                                (BWEN[0]) & \
                                (BWEN[1]) & \
                                (BWEN[2]) & \
                                (BWEN[3]) & \
                                (BWEN[4]) & \
                                (BWEN[5]) & \
                                (BWEN[6]) & \
                                (BWEN[7]) & \
                                (BWEN[8]) & \
                                (BWEN[9]) & \
                                (BWEN[10]) & \
                                (BWEN[11]) & \
                                (BWEN[12]) & \
                                (BWEN[13]) & \
                                (BWEN[14]) & \
                                (BWEN[15]) & \
                                (BWEN[16]) & \
                                (BWEN[17]) & \
                                (BWEN[18]) & \
                                (BWEN[19]) & \
                                (BWEN[20]) & \
                                (BWEN[21]) & \
                                (BWEN[22]) & \
                                (BWEN[23]) & \
                                (BWEN[24]) & \
                                (BWEN[25]) & \
                                (BWEN[26]) & \
                                (BWEN[27]) & \
                                (BWEN[28]) & \
                                (BWEN[29]) & \
                                (BWEN[30]) & \
                                (BWEN[31]) \
                                ) \
                                 ";
                        rise_power(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
                        index_1 ("0.000, 1.000");
                        values ("17.259, 17.259")
                        }
                        fall_power(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
                        index_1 ("0.000, 1.000");
                        values ("0.000, 0.000")
                        }
                }
                internal_power(){
                        when : "(!CEN & \
                                   WEN & \
                                (BWEN[0]) & \
                                (BWEN[1]) & \
                                (BWEN[2]) & \
                                (BWEN[3]) & \
                                (BWEN[4]) & \
                                (BWEN[5]) & \
                                (BWEN[6]) & \
                                (BWEN[7]) & \
                                (BWEN[8]) & \
                                (BWEN[9]) & \
                                (BWEN[10]) & \
                                (BWEN[11]) & \
                                (BWEN[12]) & \
                                (BWEN[13]) & \
                                (BWEN[14]) & \
                                (BWEN[15]) & \
                                (BWEN[16]) & \
                                (BWEN[17]) & \
                                (BWEN[18]) & \
                                (BWEN[19]) & \
                                (BWEN[20]) & \
                                (BWEN[21]) & \
                                (BWEN[22]) & \
                                (BWEN[23]) & \
                                (BWEN[24]) & \
                                (BWEN[25]) & \
                                (BWEN[26]) & \
                                (BWEN[27]) & \
                                (BWEN[28]) & \
                                (BWEN[29]) & \
                                (BWEN[30]) & \
                                (BWEN[31]) \
                                ) \
                                 ";
                        rise_power(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
                        index_1 ("0.000, 1.000");
                        values ("15.400, 15.400")
                        }
                        fall_power(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
                        index_1 ("0.000, 1.000");
                        values ("0.000, 0.000")
                        }
                }
                internal_power(){
                        when : "(CEN)";
                        rise_power(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
                        index_1 ("0.000, 1.000");
                        values ("0.021, 0.021")
                        }
                        fall_power(S55NLLGSPH_X512Y16D32_BW_energy_template_clkslew) {
                        index_1 ("0.000, 1.000");
                        values ("0.000, 0.000")
                        }
                }
        }

        pin(CEN)   {
                direction : input;
                capacitance : 0.023;
                timing() {
                        related_pin     : CLK;
                        timing_type     : setup_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.171, 0.171, 0.173, 0.175, 0.176, 0.178, 0.175",\
			"0.167, 0.171, 0.173, 0.175, 0.176, 0.178, 0.175",\
			"0.166, 0.174, 0.175, 0.177, 0.179, 0.181, 0.177",\
			"0.169, 0.182, 0.184, 0.186, 0.188, 0.188, 0.182",\
			"0.178, 0.203, 0.205, 0.207, 0.209, 0.210, 0.206",\
			"0.198, 0.254, 0.255, 0.258, 0.260, 0.261, 0.256",\
			"0.250, 0.317, 0.318, 0.320, 0.321, 0.322, 0.318"\
                        );
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.180, 0.180, 0.182, 0.186, 0.196, 0.211, 0.242",\
			"0.272, 0.180, 0.182, 0.186, 0.196, 0.213, 0.242",\
			"0.271, 0.182, 0.184, 0.188, 0.198, 0.214, 0.243",\
			"0.273, 0.190, 0.192, 0.197, 0.206, 0.221, 0.251",\
			"0.281, 0.206, 0.207, 0.212, 0.221, 0.237, 0.265",\
			"0.298, 0.233, 0.235, 0.240, 0.248, 0.264, 0.295",\
			"0.326, 0.258, 0.259, 0.263, 0.272, 0.289, 0.320"\
                        );
                        }
                } 
                timing() {
                        related_pin     : CLK;
                        timing_type     : hold_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.030, 0.030, 0.027, 0.025, 0.024, 0.023, 0.026",\
			"0.033, 0.030, 0.027, 0.025, 0.024, 0.023, 0.025",\
			"0.033, 0.027, 0.025, 0.022, 0.021, 0.020, 0.023",\
			"0.031, 0.019, 0.016, 0.014, 0.013, 0.012, 0.015",\
			"0.023, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000",\
			"0.002, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000",\
			"0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000"\
                        );
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.020, 0.020, 0.018, 0.014, 0.004, 0.000, 0.000",\
			"0.000, 0.021, 0.018, 0.014, 0.004, 0.000, 0.000",\
			"0.000, 0.018, 0.016, 0.011, 0.002, 0.000, 0.000",\
			"0.000, 0.010, 0.008, 0.003, 0.000, 0.000, 0.000",\
			"0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000",\
			"0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000",\
			"0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000"\
                        );
                        }
               }
        }
       


        bus(BWEN){
                bus_type : S55NLLGSPH_X512Y16D32_BW_WRITE;
                direction : input;
                capacitance : 0.025;
                timing() {
                        related_pin     : CLK;
                        timing_type     : setup_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.181, 0.183, 0.186, 0.191, 0.197, 0.206, 0.213",\
			"0.180, 0.182, 0.185, 0.190, 0.196, 0.205, 0.212",\
			"0.180, 0.182, 0.185, 0.189, 0.196, 0.205, 0.212",\
			"0.183, 0.185, 0.188, 0.193, 0.199, 0.208, 0.215",\
			"0.195, 0.197, 0.200, 0.204, 0.210, 0.219, 0.227",\
			"0.234, 0.235, 0.238, 0.242, 0.246, 0.253, 0.260",\
			"0.288, 0.290, 0.293, 0.296, 0.300, 0.304, 0.306"\
                        );
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.158, 0.160, 0.165, 0.174, 0.188, 0.212, 0.234",\
			"0.158, 0.160, 0.164, 0.173, 0.188, 0.211, 0.233",\
			"0.158, 0.160, 0.165, 0.173, 0.188, 0.212, 0.234",\
			"0.162, 0.164, 0.169, 0.178, 0.192, 0.216, 0.238",\
			"0.177, 0.179, 0.183, 0.192, 0.207, 0.230, 0.253",\
			"0.215, 0.217, 0.222, 0.231, 0.245, 0.269, 0.291",\
			"0.270, 0.272, 0.277, 0.285, 0.300, 0.323, 0.346"\
                        );
                        }
                } 
                timing() {
                        related_pin     : CLK;
                        timing_type     : hold_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.164, 0.163, 0.160, 0.156, 0.152, 0.148, 0.147",\
			"0.165, 0.163, 0.161, 0.157, 0.152, 0.148, 0.147",\
			"0.164, 0.163, 0.160, 0.156, 0.152, 0.148, 0.147",\
			"0.160, 0.158, 0.156, 0.152, 0.149, 0.146, 0.144",\
			"0.147, 0.145, 0.142, 0.139, 0.137, 0.136, 0.134",\
			"0.116, 0.137, 0.137, 0.137, 0.133, 0.136, 0.122",\
			"0.073, 0.137, 0.136, 0.137, 0.133, 0.137, 0.122"\
                        );
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.189, 0.187, 0.182, 0.175, 0.163, 0.151, 0.154",\
			"0.190, 0.188, 0.183, 0.176, 0.164, 0.151, 0.151",\
			"0.190, 0.188, 0.184, 0.176, 0.164, 0.151, 0.141",\
			"0.187, 0.185, 0.181, 0.173, 0.161, 0.151, 0.141",\
			"0.175, 0.173, 0.169, 0.161, 0.154, 0.151, 0.141",\
			"0.142, 0.154, 0.154, 0.154, 0.151, 0.153, 0.141",\
			"0.096, 0.154, 0.153, 0.154, 0.151, 0.154, 0.141"\
                        );
                        }
               }
        }
        pin(WEN){
                direction : input;
                capacitance : 0.021;
                timing() {
                        related_pin     : CLK;
                        timing_type     : setup_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.184, 0.186, 0.189, 0.193, 0.200, 0.208, 0.215",\
			"0.183, 0.185, 0.188, 0.192, 0.199, 0.207, 0.214",\
			"0.183, 0.184, 0.187, 0.192, 0.198, 0.207, 0.214",\
			"0.186, 0.187, 0.190, 0.195, 0.201, 0.210, 0.217",\
			"0.197, 0.199, 0.202, 0.207, 0.213, 0.222, 0.229",\
			"0.235, 0.236, 0.239, 0.243, 0.247, 0.255, 0.262",\
			"0.290, 0.291, 0.294, 0.298, 0.302, 0.305, 0.308"\
                        );
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.160, 0.162, 0.166, 0.175, 0.190, 0.213, 0.236",\
			"0.159, 0.161, 0.165, 0.174, 0.189, 0.212, 0.235",\
			"0.159, 0.161, 0.166, 0.175, 0.189, 0.213, 0.235",\
			"0.164, 0.166, 0.170, 0.179, 0.194, 0.217, 0.240",\
			"0.178, 0.180, 0.185, 0.193, 0.208, 0.232, 0.254",\
			"0.216, 0.218, 0.223, 0.232, 0.246, 0.270, 0.293",\
			"0.272, 0.274, 0.278, 0.287, 0.302, 0.325, 0.348"\
                        );
                        }
                } 
                timing() {
                        related_pin     : CLK;
                        timing_type     : hold_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.054, 0.052, 0.049, 0.045, 0.040, 0.035, 0.039",\
			"0.056, 0.054, 0.051, 0.046, 0.042, 0.037, 0.034",\
			"0.056, 0.055, 0.052, 0.047, 0.042, 0.037, 0.035",\
			"0.051, 0.051, 0.050, 0.045, 0.040, 0.035, 0.033",\
			"0.035, 0.038, 0.038, 0.039, 0.039, 0.034, 0.023",\
			"0.004, 0.038, 0.038, 0.039, 0.034, 0.037, 0.019",\
			"0.000, 0.038, 0.037, 0.039, 0.034, 0.038, 0.019"\
                        );
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
			"0.071, 0.068, 0.064, 0.054, 0.054, 0.049, 0.054",\
			"0.072, 0.070, 0.065, 0.055, 0.054, 0.049, 0.049",\
			"0.073, 0.070, 0.066, 0.056, 0.051, 0.049, 0.035",\
			"0.070, 0.069, 0.064, 0.054, 0.052, 0.049, 0.035",\
			"0.054, 0.054, 0.053, 0.054, 0.054, 0.049, 0.035",\
			"0.024, 0.052, 0.052, 0.054, 0.049, 0.051, 0.035",\
			"0.000, 0.052, 0.051, 0.054, 0.049, 0.052, 0.035"\
                        );
                        }
               }
        }
        bus(A)   {
                bus_type : S55NLLGSPH_X512Y16D32_BW_ADDRESS;
                direction : input;
                capacitance : 0.026;
                timing() {
                        related_pin     : CLK;
                        timing_type     : setup_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.227, 0.229, 0.232, 0.238, 0.245, 0.260, 0.275",\
			"0.227, 0.228, 0.231, 0.237, 0.245, 0.259, 0.274",\
			"0.227, 0.229, 0.232, 0.237, 0.245, 0.259, 0.273",\
			"0.232, 0.233, 0.236, 0.242, 0.250, 0.262, 0.277",\
			"0.246, 0.248, 0.251, 0.256, 0.264, 0.275, 0.288",\
			"0.285, 0.286, 0.290, 0.295, 0.303, 0.313, 0.322",\
			"0.336, 0.337, 0.341, 0.346, 0.354, 0.364, 0.372"\
			);
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.248, 0.250, 0.254, 0.262, 0.276, 0.299, 0.320",\
			"0.247, 0.249, 0.253, 0.261, 0.275, 0.298, 0.319",\
			"0.248, 0.249, 0.253, 0.262, 0.276, 0.298, 0.320",\
			"0.252, 0.254, 0.258, 0.266, 0.280, 0.303, 0.324",\
			"0.267, 0.268, 0.272, 0.281, 0.295, 0.317, 0.338",\
			"0.305, 0.307, 0.311, 0.319, 0.333, 0.356, 0.377",\
			"0.356, 0.358, 0.362, 0.370, 0.384, 0.407, 0.428"\
			);
                        }
                } 
                timing() {
                        related_pin     : CLK;
                        timing_type     : hold_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.110, 0.110, 0.112, 0.112, 0.107, 0.104, 0.104",\
			"0.110, 0.110, 0.111, 0.113, 0.109, 0.105, 0.105",\
			"0.110, 0.108, 0.107, 0.112, 0.109, 0.106, 0.106",\
			"0.105, 0.104, 0.102, 0.103, 0.107, 0.103, 0.104",\
			"0.095, 0.093, 0.090, 0.086, 0.093, 0.094, 0.094",\
			"0.066, 0.073, 0.073, 0.070, 0.063, 0.074, 0.064",\
			"0.024, 0.073, 0.074, 0.070, 0.063, 0.073, 0.052"\
			);
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.114, 0.113, 0.107, 0.099, 0.085, 0.068, 0.076",\
			"0.113, 0.114, 0.109, 0.100, 0.087, 0.068, 0.068",\
			"0.110, 0.113, 0.109, 0.101, 0.087, 0.068, 0.058",\
			"0.102, 0.104, 0.108, 0.098, 0.085, 0.068, 0.058",\
			"0.092, 0.089, 0.090, 0.089, 0.076, 0.068, 0.058",\
			"0.062, 0.079, 0.079, 0.076, 0.068, 0.079, 0.058",\
			"0.020, 0.079, 0.079, 0.076, 0.068, 0.079, 0.058"\
			);
                        }
               }
                internal_power(){
                        when : "CEN";
                        rise_power(S55NLLGSPH_X512Y16D32_BW_energy_template_sigslew) {
                        index_1 ("0.000, 1.000");
                        values ("0.475, 0.475")
                        }
                        fall_power(S55NLLGSPH_X512Y16D32_BW_energy_template_sigslew) {
                        index_1 ("0.000, 1.000");
                        values ("0.475, 0.475")
                        }
                }
        }
        bus(D)   {
                bus_type : S55NLLGSPH_X512Y16D32_BW_DATA;
                memory_write() {
                        address : A;
                        clocked_on : "CLK";
                }
                direction : input;
                capacitance : 0.025;
                timing() {
                        related_pin     : CLK;
                        timing_type     : setup_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.183, 0.185, 0.188, 0.192, 0.199, 0.208, 0.215",\
			"0.182, 0.184, 0.187, 0.191, 0.198, 0.207, 0.214",\
			"0.182, 0.184, 0.187, 0.191, 0.198, 0.207, 0.214",\
			"0.185, 0.187, 0.190, 0.194, 0.201, 0.210, 0.217",\
			"0.197, 0.198, 0.201, 0.206, 0.212, 0.221, 0.228",\
			"0.235, 0.237, 0.240, 0.243, 0.248, 0.254, 0.261",\
			"0.290, 0.291, 0.294, 0.298, 0.302, 0.305, 0.307"\
			);
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.159, 0.161, 0.166, 0.175, 0.189, 0.213, 0.235",\
			"0.159, 0.161, 0.165, 0.174, 0.188, 0.212, 0.234",\
			"0.159, 0.161, 0.166, 0.174, 0.189, 0.213, 0.235",\
			"0.163, 0.165, 0.170, 0.179, 0.193, 0.217, 0.239",\
			"0.178, 0.180, 0.184, 0.193, 0.208, 0.231, 0.254",\
			"0.216, 0.218, 0.223, 0.232, 0.246, 0.270, 0.292",\
			"0.271, 0.273, 0.278, 0.286, 0.301, 0.324, 0.347"\
			);
                        }
                } 
                timing() {
                        related_pin     : CLK;
                        timing_type     : hold_rising ;
                        rise_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.163, 0.161, 0.158, 0.155, 0.150, 0.146, 0.145",\
			"0.163, 0.162, 0.159, 0.155, 0.151, 0.147, 0.146",\
			"0.163, 0.161, 0.159, 0.155, 0.151, 0.148, 0.146",\
			"0.159, 0.157, 0.154, 0.150, 0.148, 0.146, 0.144",\
			"0.145, 0.144, 0.141, 0.138, 0.136, 0.135, 0.134",\
			"0.115, 0.136, 0.136, 0.136, 0.133, 0.135, 0.122",\
			"0.072, 0.136, 0.135, 0.136, 0.133, 0.136, 0.122"\
			);
                        }
                        fall_constraint(S55NLLGSPH_X512Y16D32_BW_constraint_template) {
                        index_1("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        index_2("0.010, 0.020, 0.040, 0.080, 0.160, 0.320, 0.500");
                        values(\
                        "0.187, 0.185, 0.181, 0.174, 0.162, 0.151, 0.154",\
			"0.188, 0.186, 0.182, 0.175, 0.163, 0.151, 0.151",\
			"0.189, 0.187, 0.182, 0.175, 0.163, 0.151, 0.141",\
			"0.185, 0.184, 0.179, 0.172, 0.160, 0.151, 0.141",\
			"0.174, 0.172, 0.168, 0.160, 0.154, 0.151, 0.141",\
			"0.141, 0.154, 0.154, 0.154, 0.151, 0.153, 0.141",\
			"0.095, 0.154, 0.153, 0.154, 0.151, 0.154, 0.141"\
			);
                        }
               }
        }

        cell_leakage_power : 1.639130;
}
}